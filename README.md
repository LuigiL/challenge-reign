# CHALLENGE REIGN

Under the hood
--------
- NestJS
- MongoDB
- Mongoose
- ReactJS
- Nginx 
- Docker
- docker-compose

## Prerequisites
- Docker
- Node JS 

Getting Started
---------------
```
# clone repository
$ git clone https://gitlab.com/LuigiL/challenge-reign.git
$ cd challenge-reign
```
First run
```
# install npm dependencies
$ cd server
$ npm install
$ cd ..
$ cd client
$ npm install
$ cd ..
$ docker-compose up
 
# populate database
$ docker ps
$ docker exec -it [CONTAINER ID] sh 
$ npx nestjs-command seed:posts
```

Regular run
```
# run your app (you can stop it with CTRL+C)
$ docker-compose up

# kill containers (DB data will be lost)
$ docker-compose down -v
```

Open browser in localhost:3000