import { useCallback, useEffect, useState } from "react";
import PostList from './components/PostList';
import Header from './components/Header';
import GlobalStyle from './GlobalStyle';


function App() {
  const [posts, setPosts] = useState([]);

  const getPosts = useCallback(() => {
    fetch("http://localhost:9000").then(res => res.json()).then(data => setPosts(data.posts))
  }, [setPosts])

  useEffect(() => {
    getPosts()
  }, [getPosts])

  const handleDelete = useCallback((id) => {
    fetch(`http://localhost:9000/${id}`, { method: 'DELETE' })
      .then(() => {
        getPosts()
      })
  }, [getPosts])

  return (
    <div className="App">
      <Header />
      <PostList posts={posts} onDelete={handleDelete} />
      <GlobalStyle />
    </div>
  );
}

export default App;
