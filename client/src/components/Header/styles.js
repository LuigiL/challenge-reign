import styled from 'styled-components';

export const Container = styled.div`
  background-color: #333;
  color: white;
  padding: 50px 40px;

  h1 {
    font-size: 60px;
  }

  p {
    font-size: 20px;
  }
`;
