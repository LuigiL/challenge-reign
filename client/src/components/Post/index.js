import React from 'react';
import { Container } from './styles';
import { FaTrash } from 'react-icons/fa';
import { isSameDay, parseISO, subDays } from 'date-fns';

function humanReadableDate(comparisonDate) {
    const today = new Date();
    const yesterday = subDays(today, 1);
    const compare = parseISO(comparisonDate);
    let result = '';
    if (isSameDay(compare, today)) {
        const date = new Date(comparisonDate);
        const options = { hour: "numeric", minute: "numeric", hour12: true };
        return new Intl.DateTimeFormat("en-US", options).format(date).toLowerCase();

    } else if (isSameDay(compare, yesterday)) {
        result = 'Yesterday';
    } else {
        const date = new Date(comparisonDate);
        const options = { day: "numeric", month: "long" };
        return new Intl.DateTimeFormat("en-US", options).format(date);
    }
    return result;
}


function Post({ _id, title, url, author, createdAt, onDelete }) {

    return (

        <Container >
            <div className="link" onClick={() => window.open(url, "_blank")}>
                <div>
                    <span className="title">
                        {title}
                    </span>
                    <span className="author">
                        - {author} -
                </span>
                </div>

                <span className="time">
                    {humanReadableDate(createdAt)}
                </span>
            </div>
            <span className="icon-trash" onClick={() => onDelete(_id)}>
                <FaTrash />
            </span>
        </Container >
    )
}

export default Post;