import styled from 'styled-components';

export const Container = styled.div`
  background-color: #fff;
  border: 1px solid #ccc;
  padding: 15px 10px;
  cursor: pointer;
  display: flex;
  position: relative;
  align-items: center;
  justify-content: space-between;

  &:hover {
    background-color: #fafafa; 

    .icon-trash {
      opacity: 1;
    }
  }

  > div {
    display: flex;
  }

  .title, .time {
    color: #333; 
    margin-right: 10px;
  }

  .time {
    margin-right: 50px;
  }

  .author {
    color: #999;
  }

  .link {
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  .icon-trash {
    display: flex;
    justify-content: center;
    opacity: 0;
    right: 0;
    cursor: pointer;
    z-index: 2;
    position: absolute;
    width: 50px;
    /* display: block; */
    transition: opacity 0.3s;
  }
`;
