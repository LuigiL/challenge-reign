import React from 'react';
import Post from '../Post';
import { Container } from './styles';

function PostList({ posts, onDelete }) {
    return (
        <Container>
            {posts.map(post =>
                <Post
                    key={post._id}
                    _id={post._id}
                    url={post.url}
                    title={post.title}
                    author={post.author}
                    createdAt={post.created_at}
                    onDelete={onDelete} />)}
        </Container>
    )
}

export default PostList;