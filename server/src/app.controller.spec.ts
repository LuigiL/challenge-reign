import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { PostsService } from './posts.service';
import { HttpModule } from '@nestjs/common';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { MongooseModule } from '@nestjs/mongoose';
import { PostSchema } from './post.schema';
// import { Post } from './post.inteface';

let mongod: MongoMemoryServer;

describe('App Controller', () => {
  let appController: AppController;
  let postsService: PostsService;
  let module: TestingModule;

  beforeAll((done) => {
    done();
  });

  afterAll(async (done) => {
    // await mongod.stop();
    await module.close();
    done();
  });

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [
        HttpModule,
        MongooseModule.forRootAsync({
          useFactory: async () => {
            mongod = new MongoMemoryServer();
            const mongoUri = await mongod.getUri();
            return {
              uri: mongoUri,
            };
          },
        }),

        MongooseModule.forFeature([{ name: 'Post', schema: PostSchema }]),
      ],
      controllers: [AppController],
      providers: [PostsService],
    }).compile();

    appController = module.get<AppController>(AppController);
    postsService = module.get<PostsService>(PostsService);
  });

  it('app controller should be defined', () => {
    expect(appController).toBeDefined();
  });

  it('posts service should be defined', () => {
    expect(postsService).toBeDefined();
  });

  it('should get posts', async () => {
    jest.spyOn(postsService, 'getPosts').mockReturnValue(null);
    expect(await appController.getPosts()).toEqual({ posts: null });
  });

  it('should get posts', async () => {
    jest.spyOn(postsService, 'getPosts').mockReturnValue(null);
    expect(await appController.getPosts()).toEqual({ posts: null });
  });

  it('should remove post', async () => {
    jest.spyOn(postsService, 'deletePost').mockReturnValue(null);
    expect(await appController.deletePost({ id: '1' })).toEqual({
      removed: false,
    });
  });

  it('should remove all post', async () => {
    jest.spyOn(postsService, 'deletePosts').mockReturnValue(null);
    expect(await appController.deletePosts()).toEqual({ removed: true });
  });
});
