import { Controller, Delete, Get, Param } from '@nestjs/common';
import { PostsService } from './posts.service';

@Controller()
export class AppController {
  constructor(private postsService: PostsService) {}
  @Get()
  async getPosts() {
    const posts = await this.postsService.getPosts();

    return {
      posts,
    };
  }

  @Delete('/deleteAll')
  async deletePosts() {
    await this.postsService.deletePosts();

    return { removed: true };
  }

  @Delete('/:id')
  async deletePost(@Param() params) {
    const removed = await this.postsService.deletePost(params.id);

    return { removed: !!removed };
  }
}
