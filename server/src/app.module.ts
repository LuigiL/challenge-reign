import { Module, HttpModule } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { TasksService } from './tasks.service';
import { PostsService } from './posts.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PostSchema } from './post.schema';
import config from '../config';
import { CommandModule } from 'nestjs-command';
import { PostsSeed } from './posts.seed';

@Module({
  imports: [
    CommandModule,
    HttpModule,
    ScheduleModule.forRoot(),
    MongooseModule.forRoot(config.dbUrl),
    MongooseModule.forFeature([{ name: 'Post', schema: PostSchema }]),
  ],
  controllers: [AppController],
  providers: [PostsService, TasksService, PostsSeed],
  exports: [PostsSeed],
})
export class AppModule {}
