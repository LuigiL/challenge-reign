export interface PostDto {
  readonly id_hn: string;
  readonly title: string;
  readonly url: string;
  readonly created_at: string;
  readonly author: string;
}

export interface HnPostDto {
  readonly objectID: string;
  readonly story_title: string;
  readonly story_url: string;
  readonly title: string;
  readonly url: string;
  readonly created_at: string;
  readonly author: string;
}
