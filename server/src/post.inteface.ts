import { Document } from 'mongoose';

export interface Post extends Document {
  id_hn: string;
  title: string;
  url: string;
  created_at: string;
  author: string;
  removed: boolean;
}
