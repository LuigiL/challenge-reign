import { Schema } from 'mongoose';

export const PostSchema = new Schema({
  id_hn: String,
  title: String,
  url: String,
  created_at: Date,
  author: String,
  removed: {
    type: Boolean,
    default: false,
  },
});
