import { Command } from 'nestjs-command';
import { Injectable, HttpService, Logger } from '@nestjs/common';
import { PostsService } from './posts.service';

@Injectable()
export class PostsSeed {
  private readonly logger = new Logger(PostsSeed.name);
  constructor(
    private http: HttpService,
    private readonly postsService: PostsService,
  ) {}

  @Command({ command: 'seed:posts', describe: 'seed posts', autoExit: true })
  async create() {
    const { data } = await this.http
      .get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();
    await this.postsService.savePosts(data.hits);
  }
}
