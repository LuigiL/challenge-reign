import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';
import { HttpModule } from '@nestjs/common';
import { MongoMemoryServer } from 'mongodb-memory-server';

import { PostSchema } from './post.schema';
import { PostsService } from './posts.service';

let mongod: MongoMemoryServer;

describe('Posts Service', () => {
  let postsService: PostsService;
  let module: TestingModule;

  beforeAll((done) => {
    done();
  });

  afterAll(async (done) => {
    await mongod.stop();
    await module.close();
    done();
  });

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [
        HttpModule,
        MongooseModule.forRootAsync({
          useFactory: async () => {
            mongod = new MongoMemoryServer();
            const mongoUri = await mongod.getUri();
            return {
              uri: mongoUri,
            };
          },
        }),

        MongooseModule.forFeature([{ name: 'Post', schema: PostSchema }]),
      ],
      providers: [PostsService],
    }).compile();
    postsService = module.get(PostsService);
  });
  it('Posts service should be defined', () => {
    expect(postsService).toBeDefined();
  });

  it('should get posts', async () => {
    await postsService.savePosts([
      {
        objectID: '5fab0d62fea10b017ae0bc50',
        title: 'Using Vue as a Node.js Static Site Generator',
        story_title: null,
        story_url: null,
        url:
          'http://thecodebarbarian.com/using-vue-as-a-node-js-static-site-generator.html',
        created_at: '2020-11-10T19:00:58.000Z',
        author: 'code_barbarian',
      },
      {
        objectID: '5fab0ba2e5c4df010c34e8d4',
        title: "On Apple's Piss-Poor Documentation",
        story_title: null,
        story_url: null,
        url:
          'https://www.caseyliss.com/2020/11/10/on-apples-pisspoor-documentation',
        created_at: '2020-11-10T16:01:07.000Z',
        author: 'blinkingled',
      },
    ]);

    const resultB = await postsService.getPosts();
    expect(resultB).toHaveLength(2);
  });

  it('should save posts', async () => {
    await postsService.savePosts([
      {
        objectID: '5fab0d62fea10b017ae0bc50',
        title: 'Using Vue as a Node.js Static Site Generator',
        story_title: null,
        story_url: null,
        url:
          'http://thecodebarbarian.com/using-vue-as-a-node-js-static-site-generator.html',
        created_at: '2020-11-10T19:00:58.000Z',
        author: 'code_barbarian',
      },
      {
        objectID: '5fab0ba2e5c4df010c34e8d4',
        title: "On Apple's Piss-Poor Documentation",
        story_title: null,
        story_url: null,
        url:
          'https://www.caseyliss.com/2020/11/10/on-apples-pisspoor-documentation',
        created_at: '2020-11-10T16:01:07.000Z',
        author: 'blinkingled',
      },
    ]);
    const resultB = await postsService.getPosts();
    expect(resultB).toHaveLength(2);
  });

  it('should remove all posts', async () => {
    const result = await postsService.deletePosts();
    expect(result).toBe(undefined);
  });

  it('should remove post', async () => {
    const post = await postsService.savePosts([
      {
        objectID: '5fab0d62fea10b017ae0bc50',
        title: 'Using Vue as a Node.js Static Site Generator',
        story_title: null,
        story_url: null,
        url:
          'http://thecodebarbarian.com/using-vue-as-a-node-js-static-site-generator.html',
        created_at: '2020-11-10T19:00:58.000Z',
        author: 'code_barbarian',
      },
    ]);

    await postsService.deletePost(post[0]._id);
    const resultB = await postsService.getPosts();
    expect(resultB).toHaveLength(0);
  });
});
