import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Post } from './post.inteface';
import { HnPostDto } from './post.dto';

@Injectable()
export class PostsService {
  constructor(@InjectModel('Post') private readonly postModel: Model<Post>) {}

  async getPosts(): Promise<Post[]> {
    const posts = await this.postModel
      .find({ removed: false }, { removed: 0, __v: 0, id_hn: 0 })
      .sort({ created_at: -1 });
    return posts;
  }

  async savePosts(hnPostDtos: HnPostDto[]): Promise<Post[]> {
    const oldPosts = await this.postModel.find();

    const newPosts = hnPostDtos.map((post) => {
      return new this.postModel({
        id_hn: post.objectID,
        title: post.title || post.story_title,
        url: post.url || post.story_url,
        created_at: post.created_at,
        author: post.author,
      });
    });

    const filteredPosts = newPosts.filter(
      (newPost) => newPost.title && newPost.url,
    );

    const postsToSave = filteredPosts.filter(
      (n) => !oldPosts.some((n2) => n.id_hn == n2.id_hn),
    );

    const posts = await this.postModel.insertMany(postsToSave);
    return posts;
  }

  async deletePosts(): Promise<void> {
    await this.postModel.deleteMany({});
  }

  async deletePost(id: string): Promise<Post> {
    const post = await this.postModel.findById(id);
    post.removed = true;

    const savedPost = await post.save();
    return savedPost;
  }
}
