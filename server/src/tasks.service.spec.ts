import { Test, TestingModule } from '@nestjs/testing';
import { TasksService } from './tasks.service';
import { HttpModule } from '@nestjs/common';
import { PostsService } from './posts.service';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { MongooseModule } from '@nestjs/mongoose';
import { PostSchema } from './post.schema';
import { ScheduleModule } from '@nestjs/schedule';

let mongod: MongoMemoryServer;

describe('Task Service', () => {
  let tasksService: TasksService;
  let postsService: PostsService;
  let module: TestingModule;

  beforeAll((done) => {
    done();
  });

  afterAll(async (done) => {
    await mongod.stop();
    await module.close();
    done();
  });

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        HttpModule,
        ScheduleModule.forRoot(),
        MongooseModule.forRootAsync({
          useFactory: async () => {
            mongod = new MongoMemoryServer();
            const mongoUri = await mongod.getUri();
            return {
              uri: mongoUri,
            };
          },
        }),
        MongooseModule.forFeature([{ name: 'Post', schema: PostSchema }]),
      ],
      providers: [TasksService, PostsService],
    }).compile();
    tasksService = module.get(TasksService);
    postsService = module.get(PostsService);
  });
  it('Posts service should be defined', () => {
    expect(postsService).toBeDefined();
  });

  it('Tasks service should be defined', () => {
    expect(tasksService).toBeDefined();
  });
});
