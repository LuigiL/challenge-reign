import { Injectable, Logger, HttpService } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { PostsService } from './posts.service';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  constructor(private http: HttpService, private postsService: PostsService) {}

  @Cron(CronExpression.EVERY_HOUR)
  async handleCron() {
    const { data } = await this.http
      .get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();
    await this.postsService.savePosts(data.hits);
    this.logger.debug('Update data posts HN');
  }
}
